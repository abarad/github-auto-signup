import temp_mail
import github

if __name__ == "__main__":
    mail_adresses = temp_mail.generate_mail_addresses()
    for mail_address in mail_adresses:
        github.sign_up(mail_address)
        for inbox_metadata in temp_mail.fetch_mails(mail_address):
            if inbox_metadata:
                mail_content = temp_mail.read_mail_content(
                    mail_address,
                    inbox_metadata['id'],
                )
                verification_link = temp_mail.extract_verify_link(
                    mail_content,
                )
