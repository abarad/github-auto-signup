import requests
from bs4 import BeautifulSoup

BASE_URL = 'https://www.1secmail.com/api/v1/'


def generate_mail_addresses(
    number_of_addresses: int = 10,
):
    params = {
        'action': 'genRandomMailbox',
        'count': number_of_addresses,
    }
    response = requests.request(
        'GET',
        BASE_URL,
        params=params,
    )
    return response.json()


def fetch_mails(
    mail_address: str,
):
    mail_name, mail_host = mail_address.split('@')
    params = {
        'action': 'getMessages',
        'login': mail_name,
        'domain': mail_host,
    }
    response = requests.request(
        'GET',
        BASE_URL,
        params=params,
    )
    return response.json()


def read_mail_content(
    mail_address: str,
    mail_id: int,
):
    mail_name, mail_host = mail_address.split('@')
    params = {
        'action': 'readMessage',
        'login': mail_name,
        'domain': mail_host,
        'id': mail_id,
    }
    response = requests.request(
        'GET',
        BASE_URL,
        params=params,
    )
    return response.json()


def extract_verify_link(
    mail_content: dict,
):
    soup = BeautifulSoup(
        mail_content['body'],
        'html.parser',
    )
    for a_tag in soup.find_all('a'):
        link = a_tag.get('href')
        if 'confirm_verification' in link:
            return link
